Shader "Sprites/Brick-Diffuse"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON

			#include "UnitySprites.cginc"
			
			struct V2F {
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 scrPos	: TEXCOORD1;
			};
			
			V2F vert (appdata_t v)
			{
				V2F o;
				o.vertex = UnityFlipSprite (v.vertex, _Flip);
				o.vertex = UnityObjectToClipPos (o.vertex);
				o.texcoord = v.texcoord;
				o.scrPos = ComputeScreenPos (v.vertex);
				o.color = v.color * _Color * _RendererColor;

				#ifdef PIXELSNAP_ON
				o.vertex = UnityPixelSnap (o.vertex);
				#endif

				return o;
			}

			fixed4 frag (V2F IN) : SV_Target
			{
				float2 screenPosition = IN.vertex / _ScreenParams.xy;

				fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
				c.rgb = lerp (fixed3 (1,0,0), c.rgb, screenPosition.y);
				c.rgb *= c.a;

				return c;
			}
		ENDCG
		}
	}
}
﻿using UnityEngine;

public class DefaultCamera : MonoBehaviour
{
    #region Vars
    public Transform target = null;                     // Target to follow
    public float distance = 10.0f;                      // Default Distance
    public float maxDistance = 11f;                     // Maximum zoom Distance
    public float minDistance = 1.0f;                    // Minimum zoom Distance
    public float xSpeed = 20.0f;                        // Orbit speed (Left/Right)
    public float ySpeed = 20.0f;                        // Orbit speed (Up/Down)
    public float xMinLimit = 0f;                        // Looking left limit
    public float xMaxLimit = 360f;                       // Looking right limit
    public float yMinLimit = 0f;                        // Looking up limit
    public float yMaxLimit = 60f;                       // Looking down limit
    public float zoomRate = 100f;                       // Zoom Speed
    public float zoomDampening = 3.0f;                  // Auto Zoom speed (Higher = faster)
    public float rotationDampening = 3.0f;              // Auto Rotation speed (higher = faster)
    public float xMoveRate = 0.35f;
    public float yMoveRate = 0.35f;
    public float motionDelay = 1.5f;
    public float autoRotateRate = 0.1f;

    [SerializeField]
    [HideInInspector]
    public float[] layerCullDistances = new float[32];  // Zero values in cull distances means "use far plane distance".

    [HideInInspector]
    public bool showEditLayers = true;

    protected float xDeg = 0.0f;
    protected float yDeg = 0.0f;
    protected float currentDistance;
    protected float desiredDistance;
    protected Vector3 targetOffset;
    protected float holdTimer = 0.0f;
    protected float rotateDir = 1.0f;
    #endregion

    #region Unity methods
    void OnValidate ()
    {
        ApplyCullDistance ();
    }

    void Start ()
    {
        SetPosition (new Vector3 (0, 0, 0), new Vector3 (0, 0, 0));

        currentDistance = distance;
        desiredDistance = distance;

        rotateDir = 1.0f;

        ApplyCullDistance ();
    }

    void LateUpdate ()
    {
        if (!target)
            return;

        if (!InMotion)
            holdTimer += Time.deltaTime;

        UpdateInput ();

        Calculate ();

        if (InMotion)
            AutoRotate ();
    }

#if UNITY_EDITOR
    void OnGUI ()
    {
        //GUIStyle s = new GUIStyle (UnityEditor.EditorStyles.helpBox);
        //s.alignment = TextAnchor.MiddleCenter;
        //s.normal.textColor = Utils.Colors.orange;
        //s.fontSize = 12;
        //s.fontStyle = FontStyle.Bold;
        //GUI.Box (new Rect (10, 10, 300, 20), string.Format ("Distance to camera target: {0:0.0}m.", currentDistance), s);
    }
#endif
    #endregion

    #region Get / Set
    public bool InMotion
    {
        get { return holdTimer >= motionDelay; }
    }
    #endregion

    #region Protected methods
    protected void UpdateInput ()
    {
        if (GUIUtility.hotControl != 0)
            return;

        #region Mouse input
#if (UNITY_EDITOR || UNITY_STANDALONE)
        // If either mouse buttons are down, let the mouse govern camera position
        if (Input.GetMouseButton (0))
        {
            Vector2 deltaPos = new Vector2 (Input.GetAxis ("Mouse X") * xSpeed, Input.GetAxis ("Mouse Y") * ySpeed);
            MoveCamera (deltaPos);
            ResetHoldTimer ();

            if (deltaPos.x > 0f)
                rotateDir = 1f;
            else if (deltaPos.x < 0f)
                rotateDir = -1f;
        }

        var scrollDelta = Input.GetAxis ("Mouse ScrollWheel");
        if (scrollDelta != 0)
        {
            PinchZoom (scrollDelta);
            ResetHoldTimer ();
        }
#endif
        #endregion

        #region Touch input
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE)
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 1)
            {
                Touch touchZero = Input.GetTouch (0);
                if (touchZero.phase == TouchPhase.Moved)
                {
                    MoveCamera (touchZero.deltaPosition);

                    if (touchZero.deltaPosition.x > 0f)
                        rotateDir = 1f;
                    else if (touchZero.deltaPosition.x < 0f)
                        rotateDir = -1f;

                }
            }
            else if (Input.touchCount == 2)
            {
                DetectTouchMovement.Calculate ();
                if (Mathf.Abs (DetectTouchMovement.pinchDistanceDelta) > 0)
                    PinchZoom (DetectTouchMovement.pinchDistanceDelta * 0.005f);
            }

            ResetHoldTimer ();
        }
#endif
        #endregion
    }

    protected void Calculate ()
    {
        // lerp angles
        float currentRotationAngleX = Mathf.LerpAngle (transform.eulerAngles.y, xDeg, Time.deltaTime * rotationDampening);
        float currentRotationAngleY = Mathf.LerpAngle (transform.eulerAngles.x, yDeg, Time.deltaTime * rotationDampening);

        // Set camera rotation
        Quaternion rotation = Quaternion.Euler (currentRotationAngleY, currentRotationAngleX, 0f);

        // Clamp camera distance
        desiredDistance = Mathf.Clamp (desiredDistance, minDistance, maxDistance);

        // Calculate desired camera position
        Vector3 position = (target.position + targetOffset) - (rotation * Vector3.forward * desiredDistance);

        // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance
        currentDistance = Mathf.Lerp (currentDistance, desiredDistance, Time.deltaTime * zoomDampening);

        // Recalculate position based on the new currentDistance
        position = (target.position + targetOffset) - (rotation * Vector3.forward * currentDistance);

        //Finally Set rotation and position of camera
        transform.rotation = rotation;
        transform.position = position;
    }

    protected void SetPosition (Vector3 _rotation, Vector3 _offset)
    {
        transform.rotation = Quaternion.Euler (_rotation);
        targetOffset = _offset;

        xDeg = transform.eulerAngles.y;
        yDeg = transform.eulerAngles.x;
    }

    protected void PinchZoom (float _deltaMagnitudeDiff)
    {
        desiredDistance -= _deltaMagnitudeDiff * Time.deltaTime * zoomRate * Mathf.Abs (desiredDistance);
    }

    protected void MoveCamera (float _deltaX, float _deltaY)
    {
        var deltaPosition = new Vector2 (_deltaX, _deltaY);
        MoveCamera (deltaPosition);
    }

    protected void MoveCamera (Vector2 _deltaPosition)
    {
        xDeg += _deltaPosition.x * xMoveRate;
        yDeg -= _deltaPosition.y * yMoveRate;

        xDeg = xDeg % 360f;
        yDeg = yDeg % 360f;

        if (xDeg < 0)
            xDeg += 360f;

        xDeg = Mathf.Clamp (xDeg, xMinLimit, xMaxLimit);
        yDeg = Mathf.Clamp (yDeg, yMinLimit, yMaxLimit);
    }

    protected void AutoRotate ()
    {
        xDeg += autoRotateRate * rotateDir;
    }

    protected void ResetHoldTimer ()
    {
        holdTimer = 0f;
    }

    protected void ApplyCullDistance ()
    {
        var mCamera = GetComponent<Camera> ();
        if (!mCamera)
            return;

        mCamera.layerCullDistances = layerCullDistances;
    }
    #endregion
}
﻿namespace FreeTeam.Events
{
    public class EventWrapper
    {
        #region Vars
        public event EventHandler OnHandler;
        #endregion

        #region Public methods
        public void Invoke (Event _event)
        {
            if (OnHandler != null)
                OnHandler (_event);
        }
        #endregion
    }
}
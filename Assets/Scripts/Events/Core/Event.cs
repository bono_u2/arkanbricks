﻿namespace FreeTeam.Events
{
    public class Event
    {
        #region Vars
        protected string type;
        #endregion

        public Event (string _type)
        {
            type = _type;
        }

        #region Get / Set
        public string Type
        {
            get { return type; }
        }
        #endregion
    }
}
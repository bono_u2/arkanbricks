﻿using FreeTeam.Events;

public class EventManager : EventDispatcher
{
    #region Instance
    private static EventManager _instance;
    public static EventManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = new EventManager ();

            return _instance;
        }
    }
    #endregion

    #region Public static methods
    public static void AddEventListener (string _eventType, EventHandler _listener)
    {
        Instance.AddListener (_eventType, _listener);
    }

    public static void RemoveEventListener (string _eventType, EventHandler _listener)
    {
        Instance.RemoveListener (_eventType, _listener);
    }

    public static void DispatchEvent (Event _event)
    {
        Instance.Dispatch (_event);
    }
    #endregion
}
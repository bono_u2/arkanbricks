﻿public enum GamePhase
{
    Ready,
    Play,
    Pause,
    Win,
    Lose,
}
﻿public class BricksToDownEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "BricksToDown";
    #endregion

    public BricksToDownEvent () : base (ID)
    {
    }
}
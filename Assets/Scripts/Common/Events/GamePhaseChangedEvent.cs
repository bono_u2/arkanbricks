﻿public class GamePhaseChangedEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "GamePhaseChanged";
    #endregion

    public GamePhaseChangedEvent (GamePhase _newPhase, GamePhase _prevPhase) : base (ID)
    {
        NewPhase = _newPhase;
        PrevPhase = _prevPhase;
    }

    #region Get / Set
    public GamePhase NewPhase { get; private set; }
    public GamePhase PrevPhase { get; private set; }
    #endregion
}
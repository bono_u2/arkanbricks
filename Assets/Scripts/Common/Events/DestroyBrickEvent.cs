﻿public class DestroyBrickEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "DestroyBrick";
    #endregion

    public DestroyBrickEvent () : base (ID)
    {
    }
}
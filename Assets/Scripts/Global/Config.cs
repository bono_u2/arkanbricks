﻿using System.Collections.Generic;
using System;

[Serializable]
public class Config
{
    #region Vars
    public List<LevelConfig> Levels = new List<LevelConfig> ();
    #endregion

    #region Public static methods
    public static Config Load (string _configString)
    {
        var config = UnityEngine.JsonUtility.FromJson<Config> (_configString);

        return config;
    }
    #endregion
}

[Serializable]
public class LevelConfig
{
    #region Vars
    public string Id;
    public string Scene;
    #endregion
}
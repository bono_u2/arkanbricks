﻿using FreeTeam.Events;
using System.Collections.Generic;
using UnityEngine;

public class CacheManager : EventDispatcher
{
    #region Instance
    private static CacheManager instance_;
    private static CacheManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new CacheManager ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private Dictionary<string, Object> cache = new Dictionary<string, Object> ();
    #endregion

    public CacheManager () : base () { }

    #region Public methods
    public bool ContainsKeyInCache (string _key)
    {
        return cache.ContainsKey (_key);
    }

    public void AddObjectToCache (string _key, Object _object)
    {
        if (cache.ContainsKey (_key))
            cache[_key] = _object;
        else
            cache.Add (_key, _object);
    }

    public void RemoveObjectFromCache (string _key)
    {
        cache.Remove (_key);
    }

    public TObject GetObjectFromCache<TObject> (string _key) where TObject : Object
    {
        if (cache.ContainsKey (_key))
            return (TObject)cache[_key];

        return null;
    }

    public TObject GetObjectFromCache<TObject> (string _key, Object _default = null) where TObject : Object
    {
        if (cache.ContainsKey (_key))
            return (TObject)cache[_key];

        return (TObject)_default;
    }

    public bool GetObjectFromCache<TObject> (string _key, out TObject _object) where TObject : Object
    {
        if (cache.ContainsKey (_key))
        {
            _object = (TObject)cache[_key];
            return true;
        }

        _object = null;
        return false;
    }

    public bool GetObjectFromCache<TObject> (string _key, out TObject _object, Object _default = null) where TObject : Object
    {
        if (cache.ContainsKey (_key))
        {
            _object = (TObject)cache[_key];
            return true;
        }

        _object = (TObject)_default;
        return false;
    }

    public void ClearCache ()
    {
        cache.Clear ();
    }
    #endregion

    #region Public static methods
    public static bool ContainsKey (string _key)
    {
        return Instance.ContainsKeyInCache (_key);
    }

    public static void Add (string _key, Object _object)
    {
        Instance.AddObjectToCache (_key, _object);
    }

    public static void Remove (string _key)
    {
        Instance.RemoveObjectFromCache (_key);
    }

    public static TObject Get<TObject> (string _key) where TObject : Object
    {
        return Instance.GetObjectFromCache<TObject> (_key);
    }

    public static TObject Get<TObject> (string _key, Object _default = null) where TObject : Object
    {
        return Instance.GetObjectFromCache<TObject> (_key, _default);
    }

    public static bool Get<TObject> (string _key, out TObject _object) where TObject : Object
    {
        return Instance.GetObjectFromCache<TObject> (_key, out _object);
    }

    public static bool Get<TObject> (string _key, out TObject _object, Object _default = null) where TObject : Object
    {
        return Instance.GetObjectFromCache<TObject> (_key, out _object, _default);
    }

    public static void Clear ()
    {
        Instance.ClearCache ();
    }
    #endregion
}
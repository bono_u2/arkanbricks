﻿using UnityEngine;

public class LineController : MonoBehaviour
{
    #region Vars
    private GameController gameController;
    private BallController ballController;
    #endregion

    #region Unity methods
    void Start ()
    {
        gameController = GameController.Instance;

        ballController = FindObjectOfType<BallController> ();

        transform.position = ballController.transform.position;
    }

    void Update ()
    {
        if (Input.GetMouseButton (0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            transform.rotation = Quaternion.LookRotation (Vector3.back, mousePos - transform.position.ToVector2 ());
        }

        if (Input.GetMouseButtonUp (0))
        {
            gameController.Phase = GamePhase.Play;

            gameObject.SetActive (false);
        }
    }
    #endregion
}
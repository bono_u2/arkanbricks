﻿using UnityEngine;

public class GameController : Singleton<GameController>
{
    #region Vars
    public float delayToDown = 3.0f;

    private GamePhase phase = GamePhase.Ready;

    private float timer = 0.0f;
    #endregion

    #region Unity methods
    void Start ()
    {
        BricksCount = FindObjectsOfType<BrickController> ().Length;

        EventManager.AddEventListener (DestroyBrickEvent.ID, OnDestroyBrickHandler);
    }

    void Update ()
    {
        if (Phase == GamePhase.Play)
        {
            timer += Time.deltaTime;

            if (timer >= delayToDown)
            {
                timer = 0.0f;

                EventManager.DispatchEvent (new BricksToDownEvent ());
            }
        }
    }

    void OnDestroy ()
    {
        EventManager.RemoveEventListener (DestroyBrickEvent.ID, OnDestroyBrickHandler);
    }
    #endregion

    #region Get / Set
    public float TimeLeft
    {
        get { return delayToDown - timer; }
    }

    public int Points { get; private set; } = 0;
    public int BricksCount { get; private set; } = 0;

    public GamePhase Phase
    {
        get { return phase; }
        set
        {
            EventManager.DispatchEvent (new GamePhaseChangedEvent (value, phase));

            phase = value;
        }
    }
    #endregion

    #region Private methods
    private void OnDestroyBrickHandler (FreeTeam.Events.Event _event)
    {
        Points++;

        if (Points == BricksCount)
            Phase = GamePhase.Win;
    }
    #endregion
}
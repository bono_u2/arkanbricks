﻿using UnityEngine;

public class BrickController : MonoBehaviour
{
    #region Vars
    [Range (1, 3)]
    public int hitsToDestroy = 1;

    private int hits = 0;
    #endregion

    #region Unity methods
    void Start ()
    {
        hits = 0;

        EventManager.AddEventListener (BricksToDownEvent.ID, OnBricksToDownHandler);
    }

    void OnCollisionEnter2D (Collision2D _collision)
    {
        if (_collision.gameObject.tag == "Ball")
        {
            hits++;

            if (hits >= hitsToDestroy)
            {
                EventManager.DispatchEvent (new DestroyBrickEvent ());

                Destroy (gameObject);
            }
        }
    }

    void OnDestroy ()
    {
        EventManager.RemoveEventListener (BricksToDownEvent.ID, OnBricksToDownHandler);
    }
    #endregion

    #region Private methods
    private void OnBricksToDownHandler (FreeTeam.Events.Event _eveent)
    {
        transform.position -= Vector3.up * 0.25f;
    }
    #endregion
}
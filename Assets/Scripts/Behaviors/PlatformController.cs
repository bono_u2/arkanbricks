﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    #region Vars
    private GameController gameController;
    private BallController ballController;
    private SpriteRenderer spriteRenderer;

    private Dictionary<string, System.Action> collisionActions;

    private Vector3 position;

    private bool isDragged = false;

    private float border = 0.0f;
    #endregion

    #region Unity methods
    void Awake ()
    {
        collisionActions = new Dictionary<string, Action> () {
            { "Ball", null },
            { "Booster", null },
        };
    }

    void Start ()
    {
        gameController = GameController.Instance;

        ballController = FindObjectOfType<BallController> ();

        spriteRenderer = GetComponent<SpriteRenderer> ();

        var cameraBounds = Camera.main.OrthographicBounds ();
        var cameraPos = cameraBounds.center;
        var screenSize = cameraBounds.size;

        border = ((screenSize - Bounds.size) * 0.5f).x;
    }

    void Update ()
    {
        if (gameController.Phase != GamePhase.Play)
            return;

        if (isDragged)
        {
            Vector2 mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
            Vector2 objPosition = transform.position;
            objPosition.x = Mathf.Clamp (Camera.main.ScreenToWorldPoint (mousePosition).x, -border, border);

            transform.position = objPosition;
        }
    }

    void OnMouseDown ()
    {
        isDragged = true;
    }

    void OnMouseUp ()
    {
        isDragged = false;
    }

    void OnCollisionEnter2D (Collision2D _collision)
    {
        Action action;
        if (collisionActions.TryGetValue (_collision.gameObject.tag, out action) && action != null)
            action.Invoke ();
    }
    #endregion

    #region Get / Set
    public Bounds Bounds
    {
        get { return spriteRenderer.bounds; }
    }
    #endregion
}
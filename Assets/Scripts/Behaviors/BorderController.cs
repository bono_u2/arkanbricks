﻿using UnityEngine;

public class BorderController : MonoBehaviour
{
    #region Vars
    private Transform topBorder;
    private Transform bottomBorder;
    private Transform leftBorder;
    private Transform rightBorder;
    #endregion

    #region Unity methods
    void Start ()
    {
        var cameraBounds = Camera.main.OrthographicBounds ();
        var cameraPos = cameraBounds.center;
        var screenSize = cameraBounds.size;

        topBorder = new GameObject ("TopBorder", typeof (BoxCollider2D)).transform;
        topBorder.parent = transform;
        topBorder.tag = "Border";
        topBorder.localScale = new Vector3 (screenSize.x, 1, 1);
        topBorder.position = new Vector3 (cameraPos.x, cameraPos.y + (screenSize.y + topBorder.localScale.y) * 0.5f, 0);

        bottomBorder = new GameObject ("BottomBorder", typeof (BoxCollider2D)).transform;
        bottomBorder.parent = transform;
        bottomBorder.tag = "Border";
        bottomBorder.localScale = new Vector3 (screenSize.x, 1, 1);
        bottomBorder.position = new Vector3 (cameraPos.x, cameraPos.y - (screenSize.y + bottomBorder.localScale.y) * 0.5f, 0);

        rightBorder = new GameObject ("RightBorder", typeof (BoxCollider2D)).transform;
        rightBorder.parent = transform;
        rightBorder.tag = "Border";
        rightBorder.localScale = new Vector3 (1, screenSize.y, 1);
        rightBorder.position = new Vector3 (cameraPos.x + (screenSize.x + rightBorder.localScale.x) * 0.5f, cameraPos.y, 0);

        leftBorder = new GameObject ("LeftBorder", typeof (BoxCollider2D)).transform;
        leftBorder.parent = transform;
        leftBorder.tag = "Border";
        leftBorder.localScale = new Vector3 (1, screenSize.y, 1);
        leftBorder.position = new Vector3 (cameraPos.x - (screenSize.x + leftBorder.localScale.x) * 0.5f, cameraPos.y, 0);
    }
    #endregion
}
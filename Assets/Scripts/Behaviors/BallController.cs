﻿using System.Collections;
using UnityEngine;

public class BallController : MonoBehaviour
{
    #region Vars
    public float speed = 5.0f;

    private Vector2 velocity;

    private Rigidbody2D rb2D;
    private GameController gameController;
    private PlatformController platformController;
    private LineController lineController;

    private Material material;
    #endregion

    #region Unity methods
    void Start ()
    {
        rb2D = GetComponent<Rigidbody2D> ();

        material = GetComponent<SpriteRenderer> ().material;

        gameController = GameController.Instance;
        platformController = FindObjectOfType<PlatformController> ();
        lineController = FindObjectOfType<LineController> ();

        EventManager.AddEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }

    void FixedUpdate ()
    {
        if (gameController.Phase != GamePhase.Play)
            return;

        if (!rb2D)
            return;

        if (transform.position.y <= platformController.transform.position.y)
            gameController.Phase = GamePhase.Lose;
    }

    void OnCollisionEnter2D (Collision2D _collision)
    {
        if (_collision.transform.tag == "Platform")
        {
            foreach (ContactPoint2D missileHit in _collision.contacts)
            {
                Vector2 hitPoint = missileHit.point;

                var angle = (hitPoint.x - platformController.Bounds.center.x) / platformController.Bounds.size.x * 90.0f;

                rb2D.velocity = (Quaternion.Euler (0, 0, -angle) * Vector3.up * speed).ToVector2 ();
            }
        }
    }

    void OnDestroy ()
    {
        EventManager.RemoveEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }
    #endregion

    #region Private methods
    private void OnGamePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (GamePhaseChangedEvent)_event;

        switch (e.NewPhase)
        {
            case GamePhase.Ready:
                rb2D.isKinematic = true;
                break;
            case GamePhase.Play:
                rb2D.isKinematic = false;
                rb2D.velocity = (e.PrevPhase == GamePhase.Ready) ? (lineController.transform.rotation * Vector3.up * speed).ToVector2 () : velocity;
                break;
            case GamePhase.Pause:
                velocity = rb2D.velocity;
                rb2D.velocity = Vector2.zero;
                rb2D.isKinematic = true;
                break;
            case GamePhase.Win:
            case GamePhase.Lose:
                rb2D.isKinematic = true;
                rb2D.velocity = Vector2.zero;
                StartCoroutine (Hide ());
                break;
        }
    }
    #endregion

    #region Coroutines
    private IEnumerator Hide ()
    {
        yield return null;

        float duration = 0.5f;
        float time = duration;

        var c = material.color;

        while (time > 0.0f)
        {
            c.a = time / duration;
            material.color = c;

            time -= Time.deltaTime;

            yield return null;
        }

        c.a = 0.0f;
        material.color = c;
    }
    #endregion
}
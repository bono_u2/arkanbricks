﻿using FreeTeam.Events;

public class ConsoleLogEvent : Event
{
    #region Static vars
    public const string ID = "ConsoleLog";
    #endregion

    public ConsoleLogEvent (ConsoleLogType _logType, string _text, ConsoleChannelType _channel = ConsoleChannelType.Default) : base (ID)
    {
        LogType = _logType;
        Text = _text;
        Channel = _channel;
    }

    public ConsoleLogEvent (ConsoleLog _consoleLog) : base (ID)
    {
        LogType = _consoleLog.LogType;
        Text = _consoleLog.Text;
        Channel = _consoleLog.Channel;
    }

    #region Get / Set
    public ConsoleLogType LogType { get; private set; }
    public string Text { get; private set; }
    public ConsoleChannelType Channel { get; private set; }
    #endregion
}
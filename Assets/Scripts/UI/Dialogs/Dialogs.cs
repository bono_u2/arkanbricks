﻿using System;

public class Dialogs
{
    #region Constants
    public const string ERROR_DIALOG_NAME = "ErrorDialog/ErrorDialog";
    public const string SETTINGS_DIALOG_NAME = "SettingsDialog/SettingsDialog";
    public const string QUIT_DIALOG_NAME = "QuitDialog/QuitDialog";
    public const string PAUSE_DIALOG_NAME = "PauseDialog/PauseDialog";
    public const string START_LEVEL_DIALOG_NAME = "StartLevelDialog/StartLevelDialog";
    public const string WIN_DIALOG_NAME = "WinDialog/WinDialog";
    public const string LOSE_DIALOG_NAME = "LoseDialog/LoseDialog";
    #endregion
}
﻿using System;
using UnityEngine;

public class StartLevelDialogController : DialogController
{
    #region Public methods
    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion
}
﻿using System;

public class QuitDialogController : DialogController
{
    #region Public methods
    public void OnCancelBtnHandler ()
    {
        Close ();
    }

    public void OnExitBtnHandler ()
    {
        Result = true;

        Close ();
    }
    #endregion
}
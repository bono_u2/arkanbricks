﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialogsManager : MonoBehaviour
{
    #region Instance
    private static DialogsManager instance_;
    public static DialogsManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<DialogsManager> ();

            if (instance_ == null)
                Console.Error (string.Format ("No <b>{0}</b> instance set", typeof (DialogsManager).FullName));

            return instance_;
        }
    }
    #endregion

    #region Constants
    private const string INITIALIZATION_METHOD_NAME = "Initialization";
    private const string DESTROY_METHOD_NAME = "Destroy";
    private const string KEY_EVENT_METHOD_NAME = "KeyEvent";
    #endregion

    #region Static vars
    private const string DIALOG_PREFABS_PATH = "Ui/Dialogs/{0}";
    #endregion

    #region Vars
    private List<DialogController> dialogs = new List<DialogController> ();
    #endregion

    #region Unity methods
    void Awake ()
    {
        UiRaycaster = GetComponentInParent<UnityEngine.UI.GraphicRaycaster> ();
    }

    void OnGUI ()
    {
        Event e = Event.current;
        if (e.isKey && e.type == EventType.KeyUp)
        {
            var dialogController = dialogs.LastOrDefault ();
            if (dialogController)
                dialogController.SendMessage (KEY_EVENT_METHOD_NAME, e.keyCode);
        }
    }
    #endregion

    #region Get / Set
    public UnityEngine.UI.GraphicRaycaster UiRaycaster { get; private set; }
    #endregion

    #region Public methods
    public DialogController Create (string _dialogName, object _data = null)
    {
        return Create<DialogController> (_dialogName);
    }

    public T Create<T> (string _dialogName, object _data = null) where T : DialogController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_dialogName))
        {
            // Debug
            Console.Error ("<color=#eeeeee>(DialogsManager) DialogName is null or empty!</color>", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (DIALOG_PREFABS_PATH, _dialogName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(DialogsManager) Prefab \"<b>{0}</b>\" no found!</color>", _dialogName), ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var dialogGO = Instantiate (prefab, this.transform, false);

        // Get DialogController component
        var dialogController = dialogGO.GetComponent<T> ();
        if (!dialogController)
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(DialogsManager) Controller \"<b>{0}</b>\" in prefab \"<b>{1}</b>\" no found!</color>", typeof (T), _dialogName), ConsoleChannelType.UI);

            // Destroy dialog instance
            dialogGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        dialogs.Add (dialogController);

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(DialogsManager) Dialogs count: <b>{0}</b></color>", dialogs.Count), ConsoleChannelType.UI);

        // set dialog Data
        dialogController.Data = _data;

        // подписываемся на событие закрытия диалога
        dialogController.AddListener (DialogEventType.Destruction, OnDestructionDialogHandler);

        // Init DialogController
        dialogController.SendMessage (INITIALIZATION_METHOD_NAME);

        return dialogController;
    }

    public void Close ()
    {
        var dialogController = dialogs.Last ();

        Close (dialogController);
    }

    public void Close (DialogController _dialogController)
    {
        if (!_dialogController)
            return;

        _dialogController.Close ();
    }

    public void Destroy ()
    {
        // Last DialogController
        var dialogController = dialogs.LastOrDefault ();

        // Destroy dialog
        Destroy (dialogController);
    }

    public void Destroy (DialogController _dialogController)
    {
        // Check DialogController
        if (!_dialogController)
            return;

        // отписываемся от события
        _dialogController.RemoveListener (DialogEventType.Destruction, OnDestructionDialogHandler);

        // Call Closing DialogController
        _dialogController.SendMessage (DESTROY_METHOD_NAME);

        // Remove DialogController from dialogs list
        dialogs.Remove (_dialogController);

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(DialogsManager) Dialogs count: <b>{0}</b></color>", dialogs.Count), ConsoleChannelType.UI);

        // Destroy dialog GameObject
        GameObject.Destroy (_dialogController.gameObject);
    }

    public void DestroyAll ()
    {
        for (var i = 0; i < dialogs.Count; i++)
            Destroy (dialogs[i]);
    }
    #endregion

    #region Private methods
    private void OnDestructionDialogHandler (DialogEvent _event)
    {
        // уничтожаем диалог
        Destroy (_event.Target);
    }
    #endregion

    #region Public static methods
    public static DialogController CreateDialog (string _dialogName, object _data = null)
    {
        return DialogsManager.Instance.Create (_dialogName, _data);
    }

    public static T CreateDialog<T> (string _dialogName, object _data = null) where T : DialogController
    {
        return DialogsManager.Instance.Create<T> (_dialogName, _data);
    }

    public static void CloseDialog ()
    {
        DialogsManager.Instance.Close ();
    }

    public static void CloseDialog (DialogController _dialogController)
    {
        DialogsManager.Instance.Close (_dialogController);
    }

    public static void DestroyDialog ()
    {
        DialogsManager.Instance.Destroy ();
    }

    public static void DestroyDialog (DialogController _dialogController)
    {
        DialogsManager.Instance.Destroy (_dialogController);
    }

    public static void DestroyAllDialogs ()
    {
        DialogsManager.Instance.DestroyAll ();
    }
    #endregion
}
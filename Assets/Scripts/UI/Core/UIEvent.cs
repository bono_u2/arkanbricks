﻿using System;

public class UIEvent
{
    public UIEvent (object _target, Enum _type, object _data = null)
    {
        Target = _target;
        Type = _type;
        Data = _data;
    }

    #region Get / Set
    public object Target { get; private set; }
    public Enum Type { get; private set; }
    public object Data { get; private set; }
    #endregion
}
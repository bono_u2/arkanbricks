﻿public enum UIEventType
{
    Init,
    Close,
    Destruction,
    Show,
    Hide,
}
﻿public enum DialogEventType
{
    Init,
    Close,
    Destruction,
    Show,
    Hide,
}
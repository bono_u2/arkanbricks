﻿using TMPro;

public class TMP_OptionDataString : TMP_Dropdown.OptionData
{
    #region Vars
    public string value;
    #endregion
}

public class TMP_OptionDataInt : TMP_Dropdown.OptionData
{
    #region Vars
    public int value;
    #endregion
}

public class TMP_OptionDataFloat : TMP_Dropdown.OptionData
{
    #region Vars
    public float value;
    #endregion
}

public class TMP_OptionDataObject : TMP_Dropdown.OptionData
{
    #region Vars
    public object value;
    #endregion
}
﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    #region Instance
    private static ScreensManager instance_;
    public static ScreensManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<ScreensManager> ();

            if (instance_ == null)
                Console.Error (string.Format ("No <b>{0}</b> instance set", typeof (ScreensManager).FullName));

            return instance_;
        }
    }
    #endregion

    #region Static vars
    private const string SCREEN_PREFABS_PATH = "Ui/Screens/{0}";
    #endregion

    #region Vars
    private List<ScreenController> screens = new List<ScreenController> ();
    #endregion

    #region Unity methods
    void Awake ()
    {
        UiRaycaster = GetComponentInParent<UnityEngine.UI.GraphicRaycaster> ();
    }
    #endregion

    #region Get / Set
    public UnityEngine.UI.GraphicRaycaster UiRaycaster { get; private set; }
    #endregion

    #region Public methods
    public ScreenController Create (string _screenName, object _data = null, bool _keepHistory = true)
    {
        return Create<ScreenController> (_screenName, _data, _keepHistory);
    }

    public T Create<T> (string _screenName, object _data = null, bool _keepHistory = true) where T : ScreenController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_screenName))
        {
            // Debug
            Console.Error ("<color=#eeeeee>(ScreensManager) ScreenName is null or empty!</color>", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (SCREEN_PREFABS_PATH, _screenName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(ScreensManager) Prefab \"<b>{0}</b>\" no found!</color>", _screenName), ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var screenGO = Instantiate (prefab, transform, false);

        // Get DialogController component
        var screenController = screenGO.GetComponent<T> ();
        if (!screenController)
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(ScreensManager) Controller \"<b>{0}</b>\" in prefab \"<b>{1}</b>\" no found!</color>", typeof (T), _screenName), ConsoleChannelType.UI);

            // Destroy screen instance
            screenGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        screens.Add (screenController);

        // set dialog Data
        screenController.Data = _data;

        // подписываемся на событие закрытия диалога
        screenController.AddListener (ScreenEventType.Destruction, OnDestructionScreenHandler);

        // Init DialogController
        screenController.SendMessage ("Initialization");

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(ScreensManager) Screens count: <b>{0}</b></color>", screens.Count), ConsoleChannelType.UI);

        return screenController;
    }

    public void Destroy ()
    {
        // Last DialogController
        var screenController = screens.Last ();

        Destroy (screenController);
    }

    public void Destroy (ScreenController _screenController)
    {
        // Check DialogController
        if (!_screenController)
        {
            Console.Error ("(ScreensManager) Controller is null!", ConsoleChannelType.UI);
            return;
        }

        // отписываемся от события
        _screenController.RemoveListener (ScreenEventType.Destruction, OnDestructionScreenHandler);

        // Call Closing DialogController
        _screenController.SendMessage ("Destroy");

        // Remove DialogController from dialogs list
        screens.Remove (_screenController);

        // Destroy dialog GameObject
        GameObject.Destroy (_screenController.gameObject);

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(ScreensManager) Screens count: <b>{0}</b></color>", screens.Count), ConsoleChannelType.UI);
    }

    public void DestroyAll ()
    {
        for (var i = 0; i < screens.Count; i++)
            Destroy (screens[i]);
    }
    #endregion

    #region Private methods
    private void OnDestructionScreenHandler (ScreenEvent _event)
    {
        // уничтожаем диалог
        Destroy (_event.Target);
    }
    #endregion

    #region Public static methods
    public static ScreenController CreateScreen (string _screenName, object _data = null)
    {
        return ScreensManager.Instance.Create (_screenName, _data);
    }

    public static T CreateScreen<T> (string _screenName, object _data = null) where T : ScreenController
    {
        return ScreensManager.Instance.Create<T> (_screenName, _data);
    }

    public static void DestroyScreen ()
    {
        ScreensManager.Instance.Destroy ();
    }

    public static void DestroyScreen (ScreenController _screenController)
    {
        ScreensManager.Instance.Destroy (_screenController);
    }

    public static void DestroyAllScreeens ()
    {
        ScreensManager.Instance.DestroyAll ();
    }
    #endregion
}
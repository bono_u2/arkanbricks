﻿using System;

public static class ConvertUtils
{
    #region Public static methods
    public static string ConvertToString (this Enum _enum)
    {
        return Enum.GetName (_enum.GetType (), _enum);
    }

    public static EnumType ConvertToEnum<EnumType> (this string _enumValue)
    {
        return (EnumType)Enum.Parse (typeof (EnumType), _enumValue);
    }

    public static int GetCount (this Enum _enum)
    {
        return Enum.GetNames (_enum.GetType ()).Length;
    }
    #endregion
}
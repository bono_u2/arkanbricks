﻿using UnityEngine;

public class ShaderUtils
{
    public static int COLOR_PROP_ID = Shader.PropertyToID ("_Color");
    public static int SPEC_COLOR_PROP_ID = Shader.PropertyToID ("_SpecColor");
    public static int FIRST_COLOR_PROP_ID = Shader.PropertyToID ("_FirstColor");
    public static int SECOND_COLOR_PROP_ID = Shader.PropertyToID ("_SecondColor");
    public static int VINYL_COLOR_PROP_ID = Shader.PropertyToID ("_VinylColor");
    public static int BACK_TEX_PROP_ID = Shader.PropertyToID ("_BackTex");
    public static int MAIN_TEX_PROP_ID = Shader.PropertyToID ("_MainTex");
    public static int FRONT_TEX_PROP_ID = Shader.PropertyToID ("_FrontTex");
    public static int MASK_TEX_PROP_ID = Shader.PropertyToID ("_MaskTex");
    public static int NUMBER_TEX_PROP_ID = Shader.PropertyToID ("_NumTex");
    public static int AUX_TEX_PROP_ID = Shader.PropertyToID ("_AuxTex");
    public static int OCCLUSION_TEX_PROP_ID = Shader.PropertyToID ("_OcclusionTex");
    public static int SPARK_TEX_PROP_ID = Shader.PropertyToID ("_SparkTex");
    public static int COMB_TEX_PROP_ID = Shader.PropertyToID ("_CombTex");
    public static int IDENTITY_MATRIX_PROP_ID = Shader.PropertyToID ("_IdentityMatrix");
    public static int ROTATE_MATRIX_PROP_ID = Shader.PropertyToID ("_RotateMatrix");
    public static int ROTATION_SPEED_PROP_ID = Shader.PropertyToID ("_RotationSpeed");
    public static int BLUR_PROP_ID = Shader.PropertyToID ("_Blur");
    public static int GLOSS_POWER_PROP_ID = Shader.PropertyToID ("_GlossPower");
    public static int SPEC_POWER_PROP_ID = Shader.PropertyToID ("_SpecPower");
    public static int SPARK_POWER_PROP_ID = Shader.PropertyToID ("_SparkPower");
    public static int REFL_POWER_PROP_ID = Shader.PropertyToID ("_ReflPower");
    public static int STAGE_PROP_ID = Shader.PropertyToID ("_Stage");
    public static int ALPHA_PROP_ID = Shader.PropertyToID ("_Alpha");
    public static int IS_MATTED_PROP_ID = Shader.PropertyToID ("_IsMatted");
    public static int IS_GLOW_PROP_ID = Shader.PropertyToID ("_IsGlow");
    public static int PAINT_TYPE_PROP_ID = Shader.PropertyToID ("_PaintType");
}
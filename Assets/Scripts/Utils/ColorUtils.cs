using UnityEngine;

namespace Utils
{
    public class Colors
    {
        #region Public static vars

        public static Color fuchsia = ColorUtils.HexToColorRGBA ("ff00ffff");
        public static Color grey = ColorUtils.HexToColorRGBA ("808080ff");
        public static Color maroon = ColorUtils.HexToColorRGBA ("800000FF");
        public static Color darkred = ColorUtils.HexToColorRGBA ("8B0000FF");
        public static Color brown = ColorUtils.HexToColorRGBA ("A52A2AFF");
        public static Color firebrick = ColorUtils.HexToColorRGBA ("B22222FF");
        public static Color crimson = ColorUtils.HexToColorRGBA ("DC143CFF");
        public static Color red = ColorUtils.HexToColorRGBA ("FF0000FF");
        public static Color tomato = ColorUtils.HexToColorRGBA ("FF6347FF");
        public static Color coral = ColorUtils.HexToColorRGBA ("FF7F50FF");
        public static Color indianred = ColorUtils.HexToColorRGBA ("CD5C5CFF");
        public static Color lightcoral = ColorUtils.HexToColorRGBA ("F08080FF");
        public static Color darksalmon = ColorUtils.HexToColorRGBA ("E9967AFF");
        public static Color salmon = ColorUtils.HexToColorRGBA ("FA8072FF");
        public static Color lightsalmon = ColorUtils.HexToColorRGBA ("FFA07AFF");
        public static Color orangered = ColorUtils.HexToColorRGBA ("FF4500FF");
        public static Color darkorange = ColorUtils.HexToColorRGBA ("FF8C00FF");
        public static Color orange = ColorUtils.HexToColorRGBA ("FFA500FF");
        public static Color gold = ColorUtils.HexToColorRGBA ("FFD700FF");
        public static Color darkgoldenrod = ColorUtils.HexToColorRGBA ("B8860BFF");
        public static Color goldenrod = ColorUtils.HexToColorRGBA ("DAA520FF");
        public static Color palegoldenrod = ColorUtils.HexToColorRGBA ("EEE8AAFF");
        public static Color darkkhaki = ColorUtils.HexToColorRGBA ("BDB76BFF");
        public static Color khaki = ColorUtils.HexToColorRGBA ("F0E68CFF");
        public static Color olive = ColorUtils.HexToColorRGBA ("808000FF");
        public static Color yellow = ColorUtils.HexToColorRGBA ("FFFF00FF");
        public static Color yellowgreen = ColorUtils.HexToColorRGBA ("9ACD32FF");
        public static Color darkolivegreen = ColorUtils.HexToColorRGBA ("556B2FFF");
        public static Color olivedrab = ColorUtils.HexToColorRGBA ("6B8E23FF");
        public static Color lawngreen = ColorUtils.HexToColorRGBA ("7CFC00FF");
        public static Color chartreuse = ColorUtils.HexToColorRGBA ("7FFF00FF");
        public static Color greenyellow = ColorUtils.HexToColorRGBA ("ADFF2FFF");
        public static Color darkgreen = ColorUtils.HexToColorRGBA ("006400FF");
        public static Color green = ColorUtils.HexToColorRGBA ("008000FF");
        public static Color forestgreen = ColorUtils.HexToColorRGBA ("228B22FF");
        public static Color lime = ColorUtils.HexToColorRGBA ("00FF00FF");
        public static Color limegreen = ColorUtils.HexToColorRGBA ("32CD32FF");
        public static Color lightgreen = ColorUtils.HexToColorRGBA ("90EE90FF");
        public static Color palegreen = ColorUtils.HexToColorRGBA ("98FB98FF");
        public static Color darkseagreen = ColorUtils.HexToColorRGBA ("8FBC8FFF");
        public static Color mediumspringgreen = ColorUtils.HexToColorRGBA ("00FA9AFF");
        public static Color springgreen = ColorUtils.HexToColorRGBA ("00FF7FFF");
        public static Color seagreen = ColorUtils.HexToColorRGBA ("2E8B57FF");
        public static Color mediumaquamarine = ColorUtils.HexToColorRGBA ("66CDAAFF");
        public static Color mediumseagreen = ColorUtils.HexToColorRGBA ("3CB371FF");
        public static Color lightseagreen = ColorUtils.HexToColorRGBA ("20B2AAFF");
        public static Color darkslategray = ColorUtils.HexToColorRGBA ("2F4F4FFF");
        public static Color teal = ColorUtils.HexToColorRGBA ("008080FF");
        public static Color darkcyan = ColorUtils.HexToColorRGBA ("008B8BFF");
        public static Color aqua = ColorUtils.HexToColorRGBA ("00FFFFFF");
        public static Color cyan = ColorUtils.HexToColorRGBA ("00FFFFFF");
        public static Color lightcyan = ColorUtils.HexToColorRGBA ("E0FFFFFF");
        public static Color darkturquoise = ColorUtils.HexToColorRGBA ("00CED1FF");
        public static Color turquoise = ColorUtils.HexToColorRGBA ("40E0D0FF");
        public static Color mediumturquoise = ColorUtils.HexToColorRGBA ("48D1CCFF");
        public static Color paleturquoise = ColorUtils.HexToColorRGBA ("AFEEEEFF");
        public static Color aquamarine = ColorUtils.HexToColorRGBA ("7FFFD4FF");
        public static Color powderblue = ColorUtils.HexToColorRGBA ("B0E0E6FF");
        public static Color cadetblue = ColorUtils.HexToColorRGBA ("5F9EA0FF");
        public static Color steelblue = ColorUtils.HexToColorRGBA ("4682B4FF");
        public static Color cornflowerblue = ColorUtils.HexToColorRGBA ("6495EDFF");
        public static Color deepskyblue = ColorUtils.HexToColorRGBA ("00BFFFFF");
        public static Color dodgerblue = ColorUtils.HexToColorRGBA ("1E90FFFF");
        public static Color lightblue = ColorUtils.HexToColorRGBA ("ADD8E6FF");
        public static Color skyblue = ColorUtils.HexToColorRGBA ("87CEEBFF");
        public static Color lightskyblue = ColorUtils.HexToColorRGBA ("87CEFAFF");
        public static Color midnightblue = ColorUtils.HexToColorRGBA ("191970FF");
        public static Color navy = ColorUtils.HexToColorRGBA ("000080FF");
        public static Color darkblue = ColorUtils.HexToColorRGBA ("00008BFF");
        public static Color mediumblue = ColorUtils.HexToColorRGBA ("0000CDFF");
        public static Color blue = ColorUtils.HexToColorRGBA ("0000FFFF");
        public static Color royalblue = ColorUtils.HexToColorRGBA ("4169E1FF");
        public static Color blueviolet = ColorUtils.HexToColorRGBA ("8A2BE2FF");
        public static Color indigo = ColorUtils.HexToColorRGBA ("4B0082FF");
        public static Color darkslateblue = ColorUtils.HexToColorRGBA ("483D8BFF");
        public static Color slateblue = ColorUtils.HexToColorRGBA ("6A5ACDFF");
        public static Color mediumslateblue = ColorUtils.HexToColorRGBA ("7B68EEFF");
        public static Color mediumpurple = ColorUtils.HexToColorRGBA ("9370DBFF");
        public static Color darkmagenta = ColorUtils.HexToColorRGBA ("8B008BFF");
        public static Color darkviolet = ColorUtils.HexToColorRGBA ("9400D3FF");
        public static Color darkorchid = ColorUtils.HexToColorRGBA ("9932CCFF");
        public static Color mediumorchid = ColorUtils.HexToColorRGBA ("BA55D3FF");
        public static Color purple = ColorUtils.HexToColorRGBA ("800080FF");
        public static Color thistle = ColorUtils.HexToColorRGBA ("D8BFD8FF");
        public static Color plum = ColorUtils.HexToColorRGBA ("DDA0DDFF");
        public static Color violet = ColorUtils.HexToColorRGBA ("EE82EEFF");
        public static Color magenta = ColorUtils.HexToColorRGBA ("FF00FFFF");
        public static Color orchid = ColorUtils.HexToColorRGBA ("DA70D6FF");
        public static Color mediumvioletred = ColorUtils.HexToColorRGBA ("C71585FF");
        public static Color palevioletred = ColorUtils.HexToColorRGBA ("DB7093FF");
        public static Color deeppink = ColorUtils.HexToColorRGBA ("FF1493FF");
        public static Color hotpink = ColorUtils.HexToColorRGBA ("FF69B4FF");
        public static Color lightpink = ColorUtils.HexToColorRGBA ("FFB6C1FF");
        public static Color pink = ColorUtils.HexToColorRGBA ("FFC0CBFF");
        public static Color antiquewhite = ColorUtils.HexToColorRGBA ("FAEBD7FF");
        public static Color beige = ColorUtils.HexToColorRGBA ("F5F5DCFF");
        public static Color bisque = ColorUtils.HexToColorRGBA ("FFE4C4FF");
        public static Color blanchedalmond = ColorUtils.HexToColorRGBA ("FFEBCDFF");
        public static Color wheat = ColorUtils.HexToColorRGBA ("F5DEB3FF");
        public static Color cornsilk = ColorUtils.HexToColorRGBA ("FFF8DCFF");
        public static Color lemonchiffon = ColorUtils.HexToColorRGBA ("FFFACDFF");
        public static Color lightgoldenrodyellow = ColorUtils.HexToColorRGBA ("FAFAD2FF");
        public static Color lightyellow = ColorUtils.HexToColorRGBA ("FFFFE0FF");
        public static Color saddlebrown = ColorUtils.HexToColorRGBA ("8B4513FF");
        public static Color sienna = ColorUtils.HexToColorRGBA ("A0522DFF");
        public static Color chocolate = ColorUtils.HexToColorRGBA ("D2691EFF");
        public static Color peru = ColorUtils.HexToColorRGBA ("CD853FFF");
        public static Color sandybrown = ColorUtils.HexToColorRGBA ("F4A460FF");
        public static Color burlywood = ColorUtils.HexToColorRGBA ("DEB887FF");
        public static Color tan = ColorUtils.HexToColorRGBA ("D2B48CFF");
        public static Color rosybrown = ColorUtils.HexToColorRGBA ("BC8F8FFF");
        public static Color moccasin = ColorUtils.HexToColorRGBA ("FFE4B5FF");
        public static Color navajowhite = ColorUtils.HexToColorRGBA ("FFDEADFF");
        public static Color peachpuff = ColorUtils.HexToColorRGBA ("FFDAB9FF");
        public static Color mistyrose = ColorUtils.HexToColorRGBA ("FFE4E1FF");
        public static Color lavenderblush = ColorUtils.HexToColorRGBA ("FFF0F5FF");
        public static Color linen = ColorUtils.HexToColorRGBA ("FAF0E6FF");
        public static Color oldlace = ColorUtils.HexToColorRGBA ("FDF5E6FF");
        public static Color papayawhip = ColorUtils.HexToColorRGBA ("FFEFD5FF");
        public static Color seashell = ColorUtils.HexToColorRGBA ("FFF5EEFF");
        public static Color mintcream = ColorUtils.HexToColorRGBA ("F5FFFAFF");
        public static Color slategray = ColorUtils.HexToColorRGBA ("708090FF");
        public static Color lightslategray = ColorUtils.HexToColorRGBA ("778899FF");
        public static Color lightsteelblue = ColorUtils.HexToColorRGBA ("B0C4DEFF");
        public static Color lavender = ColorUtils.HexToColorRGBA ("E6E6FAFF");
        public static Color floralwhite = ColorUtils.HexToColorRGBA ("FFFAF0FF");
        public static Color aliceblue = ColorUtils.HexToColorRGBA ("F0F8FFFF");
        public static Color ghostwhite = ColorUtils.HexToColorRGBA ("F8F8FFFF");
        public static Color honeydew = ColorUtils.HexToColorRGBA ("F0FFF0FF");
        public static Color ivory = ColorUtils.HexToColorRGBA ("FFFFF0FF");
        public static Color azure = ColorUtils.HexToColorRGBA ("F0FFFFFF");
        public static Color snow = ColorUtils.HexToColorRGBA ("FFFAFAFF");
        public static Color dimgray = ColorUtils.HexToColorRGBA ("696969FF");
        public static Color gray = ColorUtils.HexToColorRGBA ("808080FF");
        public static Color grayclear = ColorUtils.HexToColorRGBA ("80808000");
        public static Color darkgray = ColorUtils.HexToColorRGBA ("A9A9A9FF");
        public static Color silver = ColorUtils.HexToColorRGBA ("C0C0C0FF");
        public static Color lightgray = ColorUtils.HexToColorRGBA ("D3D3D3FF");
        public static Color gainsboro = ColorUtils.HexToColorRGBA ("DCDCDCFF");
        public static Color whitesmoke = ColorUtils.HexToColorRGBA ("F5F5F5FF");
        public static Color white = ColorUtils.HexToColorRGBA ("FFFFFFFF");
        public static Color whiteclear = ColorUtils.HexToColorRGBA ("FFFFFF00");
        public static Color black = ColorUtils.HexToColorRGBA ("000000FF");
        public static Color blackclear = ColorUtils.HexToColorRGBA ("00000000");

        #endregion
    }

    public class ColorUtils
    {
        #region Public static methods

        #region Convert
        public static string ColorToHexRGB (Color _color)
        {
            string hex = _color.r.ToString ("X2") + _color.g.ToString ("X2") + _color.b.ToString ("X2");
            return hex;
        }

        public static string ColorToHexRGBA (Color _color)
        {
            string hex = string.Format ("{0:X2}{1:X2}{2:X2}{3:X2}", (int)(_color.r * 255), (int)(_color.g * 255), (int)(_color.b * 255), (int)(_color.a * 255));
            return hex;
        }

        public static Color HexToColorRGB (string _hex)
        {
            byte r = byte.Parse (_hex.Substring (0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse (_hex.Substring (2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse (_hex.Substring (4, 2), System.Globalization.NumberStyles.HexNumber);
            byte a = 255;
            return new Color32 (r, g, b, a);
        }

        public static Color HexToColorRGBA (string _hex)
        {
            byte r = byte.Parse (_hex.Substring (0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse (_hex.Substring (2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse (_hex.Substring (4, 2), System.Globalization.NumberStyles.HexNumber);
            byte a = byte.Parse (_hex.Substring (6, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32 (r, g, b, a);
        }
        #endregion

        #region Color manipulation
        public static void LerpColors (ref Color[] baseImage, Color[] compositeImage, int w, int h)
        {
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    Color compositeColor = GetColor (compositeImage, x, y, w, h);
                    Color baseColor = GetColor (baseImage, x, y, w, h);
                    Color newColor = Color.Lerp (baseColor, compositeColor, compositeColor.a);
                    SetColor (ref baseImage, x, y, w, h, newColor);
                }
            }
        }

        public static Color[] FlipRight (Color[] pix, int width, int height)
        {
            Color cX;

            Color[] a = new Color[pix.Length];

            for (int y = 0; y < height; y++)
            {

                for (int x = 0; x < width; x++)
                {
                    cX = GetColor (pix, x, y, width, height);

                    SetColor (ref a, y, width - 1 - x, height, width, cX);
                }
            }

            return a;
        }
        #endregion

        #endregion

        #region Private static methods

        private static Color GetColor (Color[] pix, int x, int y, int w, int h)
        {
            return pix[y * w + x];
        }

        private static void SetColor (ref Color[] pix, int x, int y, int w, int h, Color c)
        {
            pix[y * w + x] = c;
        }

        #endregion
    }
}
﻿using UnityEngine;

public static class UnityExtensions
{
    public static Bounds OrthographicBounds (this Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2.0f;

        return new Bounds (camera.transform.position, new Vector3 (cameraHeight * screenAspect, cameraHeight, 0.0f));
    }

    public static T SafeDestroy<T> (this T _object) where T : Object
    {
        if (_object != null)
        {

            if (Application.isEditor && !Application.isPlaying)
                Object.DestroyImmediate (_object);
            else
                Object.Destroy (_object);
        }

        return _object;
    }

    public static float GetMaxTime (this AnimationCurve _curve)
    {
        return _curve.keys.Length > 0 ? _curve.keys[_curve.length - 1].time : 0f;
    }

    public static Vector2 ToVector2 (this Vector3 _vector)
    {
        return new Vector2 (_vector.x, _vector.y);
    }
}